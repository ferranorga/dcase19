

COARSE
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    InstanceNorm2d-1         [-1, 1, 128, 1001]               0
    MelSpectrogram-2         [-1, 1, 128, 1001]               0
    InstanceNorm2d-3         [-1, 1, 128, 1001]               0
            Conv2d-4         [-1, 20, 124, 997]             520
            Conv2d-5          [-1, 20, 58, 494]          10,020
         Dropout2d-6          [-1, 20, 58, 494]               0
            Linear-7                 [-1, 1000]      13,921,000
            Linear-8                    [-1, 8]           8,008
             LeNet-9                    [-1, 8]               0
================================================================

Epoch 1/10 9.04s Step 294/294: loss: 0.479259, acc: 78.397490, val_loss: 0.453634, val_acc: 78.611738
Epoch 2/10 8.89s Step 294/294: loss: 0.441253, acc: 79.668226, val_loss: 0.434007, val_acc: 81.715576
Epoch 3/10 9.03s Step 294/294: loss: 0.426832, acc: 80.875160, val_loss: 0.434407, val_acc: 80.981941
Epoch 4/10 9.20s Step 294/294: loss: 0.412969, acc: 81.603573, val_loss: 0.423005, val_acc: 81.376975
Epoch 5/10 9.03s Step 294/294: loss: 0.400439, acc: 82.140578, val_loss: 0.421487, val_acc: 81.574492
Epoch 6/10 8.91s Step 294/294: loss: 0.380456, acc: 82.959379, val_loss: 0.422576, val_acc: 82.590293
Epoch 7/10 9.00s Step 294/294: loss: 0.361732, acc: 83.974904, val_loss: 0.442062, val_acc: 81.546275
Epoch 8/10 8.89s Step 294/294: loss: 0.341963, acc: 85.165887, val_loss: 0.448944, val_acc: 81.066591
Epoch 9/10 8.91s Step 294/294: loss: 0.313760, acc: 86.266482, val_loss: 0.481887, val_acc: 81.235892
Epoch 10/10 9.08s Step 294/294: loss: 0.294744, acc: 87.484049, val_loss: 0.490283, val_acc: 81.574492


FINE
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    InstanceNorm2d-1         [-1, 1, 128, 1001]               0
    MelSpectrogram-2         [-1, 1, 128, 1001]               0
    InstanceNorm2d-3         [-1, 1, 128, 1001]               0
            Conv2d-4         [-1, 20, 124, 997]             520
            Conv2d-5          [-1, 20, 58, 494]          10,020
         Dropout2d-6          [-1, 20, 58, 494]               0
            Linear-7                 [-1, 1000]      13,921,000
            Linear-8                   [-1, 29]          29,029
             LeNet-9                   [-1, 29]               0
================================================================

Epoch 1/10 8.77s Step 294/294: loss: 0.257452, acc: 92.427288, val_loss: 0.273099, val_acc: 90.324590
Epoch 2/10 8.93s Step 294/294: loss: 0.226214, acc: 92.694232, val_loss: 0.267034, val_acc: 90.456917
Epoch 3/10 8.95s Step 294/294: loss: 0.221578, acc: 92.748501, val_loss: 0.267211, val_acc: 90.091073
Epoch 4/10 8.92s Step 294/294: loss: 0.216139, acc: 92.764635, val_loss: 0.257917, val_acc: 90.526972
Epoch 5/10 8.95s Step 294/294: loss: 0.210187, acc: 92.906907, val_loss: 0.258507, val_acc: 90.565892
Epoch 6/10 8.88s Step 294/294: loss: 0.205529, acc: 92.991977, val_loss: 0.262165, val_acc: 90.573675
Epoch 7/10 8.98s Step 294/294: loss: 0.198823, acc: 93.126916, val_loss: 0.263001, val_acc: 90.783841
Epoch 8/10 9.03s Step 294/294: loss: 0.187521, acc: 93.418795, val_loss: 0.261435, val_acc: 90.706002
Epoch 9/10 8.94s Step 294/294: loss: 0.174289, acc: 93.718007, val_loss: 0.275471, val_acc: 90.760490
Epoch 10/10 9.03s Step 294/294: loss: 0.161694, acc: 94.021619, val_loss: 0.282146, val_acc: 90.885033


COARSE+FINE
----------------------------------------------------------------
        Layer (type)               Output Shape         Param #
================================================================
    InstanceNorm2d-1         [-1, 1, 128, 1001]               0
    MelSpectrogram-2         [-1, 1, 128, 1001]               0
    InstanceNorm2d-3         [-1, 1, 128, 1001]               0
            Conv2d-4         [-1, 20, 124, 997]             520
            Conv2d-5          [-1, 20, 58, 494]          10,020
         Dropout2d-6          [-1, 20, 58, 494]               0
            Linear-7                 [-1, 1000]      13,921,000
            Linear-8                   [-1, 37]          37,037
             LeNet-9                   [-1, 37]               0
================================================================
Epoch 1/10 8.71s Step 74/74: loss: 0.376300, acc_coarse: 77.461718, acc_fine: 91.955000, val_loss: 0.351446, val_acc_coarse: 79.683973, val_acc_fine: 89.943178
Epoch 2/10 8.80s Step 74/74: loss: 0.341649, acc_coarse: 79.407699, acc_fine: 92.692764, val_loss: 0.362562, val_acc_coarse: 82.110609, val_acc_fine: 90.558106
Epoch 3/10 8.92s Step 74/74: loss: 0.330749, acc_coarse: 80.428541, acc_fine: 92.717698, val_loss: 0.361741, val_acc_coarse: 83.154627, val_acc_fine: 90.612594
Epoch 4/10 8.81s Step 74/74: loss: 0.319760, acc_coarse: 80.774139, acc_fine: 92.796902, val_loss: 0.341589, val_acc_coarse: 83.690745, val_acc_fine: 90.846112
Epoch 5/10 8.83s Step 74/74: loss: 0.311162, acc_coarse: 81.837516, acc_fine: 92.905440, val_loss: 0.338986, val_acc_coarse: 84.113995, val_acc_fine: 90.947303
Epoch 6/10 8.83s Step 74/74: loss: 0.303470, acc_coarse: 82.539345, acc_fine: 93.006644, val_loss: 0.324273, val_acc_coarse: 84.085779, val_acc_fine: 91.071845
Epoch 7/10 8.83s Step 74/74: loss: 0.296421, acc_coarse: 82.821140, acc_fine: 93.025711, val_loss: 0.328139, val_acc_coarse: 84.142212, val_acc_fine: 90.955087
Epoch 8/10 8.82s Step 74/74: loss: 0.293462, acc_coarse: 83.278392, acc_fine: 93.040379, val_loss: 0.323519, val_acc_coarse: 84.311512, val_acc_fine: 91.180820
Epoch 9/10 8.87s Step 74/74: loss: 0.285048, acc_coarse: 84.033390, acc_fine: 93.213452, val_loss: 0.322092, val_acc_coarse: 84.113996, val_acc_fine: 91.102980
Epoch 10/10 8.91s Step 74/74: loss: 0.280743, acc_coarse: 84.075925, acc_fine: 93.226652, val_loss: 0.321850, val_acc_coarse: 83.888262, val_acc_fine: 91.102980






Sense data augmentation, alpha = 0 (només fine loss):
Validate saved Model:
[0.22696192767765397, array([27.92774596, 56.39721076, 27.92774596, 56.39721076, 47.99661425,
       91.92807579])]

Sense data augmentation, alpha = 1 (només coarse loss):
Validate saved Model:
[0.35351420935604966, array([75.77756651, 10.77251724, 75.77756651, 10.77251724, 85.89164811,
       44.36055067])]

Sense data augmentation, amb shared layer:
Validate saved Model:
[0.29325612599101614, array([76.9614854 , 57.0465525 , 76.9614854 , 57.0465525 , 86.14559864,
       91.86580369])]

Amb shared layer, amb data augmentation
Validate saved Model:
[0.2889756800493322, array([77.56729015, 57.740951  , 77.56729015, 57.740951  , 86.34311535,
       92.07597148])]



