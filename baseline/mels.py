import io
import torch
from torchvision import models, transforms, datasets
import torch.utils.data as data
import numpy as np
import math
import torch.nn as nn
import torch.nn.functional as F

import os
import os.path
import pickle
import hashlib
import librosa
from scipy.io import wavfile


import librosa.display
import numpy as np



import torch.nn as nn
import torch.optim as optim



#Helper Functions borrowed from torchaudio https://github.com/pytorch/audio/blob/master/torchaudio/transforms.py

class PadTrim(object):
    """Pad/Trim a 1d-Tensor (Signal or Labels)
    Args:
        tensor (Tensor): Tensor of audio of size (n x c) or (c x n)
        max_len (int): Length to which the tensor will be padded
        channels_first (bool): Pad for channels first tensors.  Default: `True`
    """

    def __init__(self, max_len, fill_value=0, channels_first=True):
        self.max_len = max_len
        self.fill_value = fill_value
        self.len_dim, self.ch_dim = int(channels_first), int(not channels_first)

    def __call__(self, tensor):
        """
        Returns:
            Tensor: (c x n) or (n x c)
        """
        assert tensor.size(self.ch_dim) < 128, \
            "Too many channels ({}) detected, see channels_first param.".format(tensor.size(self.ch_dim))
        if self.max_len > tensor.size(self.len_dim):
            padding = [self.max_len - tensor.size(self.len_dim)
                       if (i % 2 == 1) and (i // 2 != self.len_dim)
                       else 0
                       for i in range(4)]
            with torch.no_grad():
                tensor = torch.nn.functional.pad(tensor, padding, "constant", self.fill_value)
        elif self.max_len < tensor.size(self.len_dim):
            tensor = tensor.narrow(self.len_dim, 0, self.max_len)
        return tensor

    def __repr__(self):
        return self.__class__.__name__ + '(max_len={0})'.format(self.max_len)




class MelScale(object):
    """This turns a normal STFT into a mel frequency STFT, using a conversion
       matrix.  This uses triangular filter banks.
    Args:
        n_mels (int): number of mel bins
        sr (int): sample rate of audio signal
        f_max (float, optional): maximum frequency. default: `sr` // 2
        f_min (float): minimum frequency. default: 0
        n_stft (int, optional): number of filter banks from stft. Calculated from first input
            if `None` is given.  See `n_fft` in `Spectrogram`.
    """
    def __init__(self, n_mels=128, sr=16000, f_max=None, f_min=0., n_stft=None):
        self.n_mels = n_mels
        self.sr = sr
        self.f_max = f_max if f_max is not None else sr // 2
        self.f_min = f_min
        self.fb = self._create_fb_matrix(n_stft) if n_stft is not None else n_stft

    def __call__(self, spec_f):
        if self.fb is None:
            self.fb = self._create_fb_matrix(spec_f.size(2)).to(spec_f.device)
        else:
            # need to ensure same device for dot product
            self.fb = self.fb.to(spec_f.device)
        spec_m = torch.matmul(spec_f, self.fb)  # (c, l, n_fft) dot (n_fft, n_mels) -> (c, l, n_mels)
        return spec_m

    def _create_fb_matrix(self, n_stft):
        """ Create a frequency bin conversion matrix.
        Args:
            n_stft (int): number of filter banks from spectrogram
        """

        # get stft freq bins
        stft_freqs = torch.linspace(self.f_min, self.f_max, n_stft)
        # calculate mel freq bins
        m_min = 0. if self.f_min == 0 else self._hertz_to_mel(self.f_min)
        m_max = self._hertz_to_mel(self.f_max)
        m_pts = torch.linspace(m_min, m_max, self.n_mels + 2)
        f_pts = self._mel_to_hertz(m_pts)
        # calculate the difference between each mel point and each stft freq point in hertz
        f_diff = f_pts[1:] - f_pts[:-1]  # (n_mels + 1)
        slopes = f_pts.unsqueeze(0) - stft_freqs.unsqueeze(1)  # (n_stft, n_mels + 2)
        # create overlapping triangles
        z = torch.tensor(0.)
        down_slopes = (-1. * slopes[:, :-2]) / f_diff[:-1]  # (n_stft, n_mels)
        up_slopes = slopes[:, 2:] / f_diff[1:]  # (n_stft, n_mels)
        fb = torch.max(z, torch.min(down_slopes, up_slopes))
        return fb

    def _hertz_to_mel(self, f):
        return 2595. * torch.log10(torch.tensor(1.) + (f / 700.))

    def _mel_to_hertz(self, mel):
        return 700. * (10**(mel / 2595.) - 1.)
      
class MelSpectrogram(nn.Module):
    def __init__(self, n_mels = 80, sfr=44100, n_fft=1024):
        super(MelSpectrogram, self).__init__()
        self.sfr = sfr
        self.window_stride=0.01
        self.window_size=0.02
        self.n_fft=n_fft
        self.n_mels=n_mels
        
        self.win_length = int(self.sfr * self.window_size)
        self.hop_length = int(self.sfr * self.window_stride)
        self.lowfreq = 20
        self.highfreq = self.sfr/2 - 400
        self.window = torch.hamming_window(self.win_length)
        
        self.mel = MelScale(n_mels=self.n_mels, sr=self.sfr, f_max=self.highfreq, f_min=self.lowfreq)
        self.norm = nn.InstanceNorm2d(1)

    def forward(self, x):
        
        x = x.squeeze(1)
        spec_f = torch.stft(x, n_fft=self.n_fft, hop_length=self.hop_length, 
                    win_length=self.win_length, 
                    window=self.window,
                    center=True,
                    normalized=False, onesided=True,
                    pad_mode='reflect'
                   )
        spec_f = spec_f.pow(2).sum(-1)
        x = self.mel(spec_f.transpose(1,2)).transpose(1,2)
        x = torch.log(x+0.0001)
        x = x.unsqueeze(1)
        x = self.norm(x)
        return x
      
      
    def plot_sample(self, fbank, index):
        librosa.display.specshow(fbank[index,:,:,:].view(self.n_mels,-1).numpy(),
                          y_axis='mel', x_axis='time', fmax=self.highfreq, hop_length=self.hop_length)
        plt.title('Mel spectrogram')
        plt.colorbar(format='%+2.0f dB')
        plt.tight_layout()
    

