wget "https://zenodo.org/record/2590742/files/annotations.csv?download=1" -O annotations.csv
wget "https://zenodo.org/record/2590742/files/audio.tar.gz?download=1" -O audio.tar.gz
wget "https://zenodo.org/record/2590742/files/dcase-ust-taxonomy.yaml?download=1" -O dcase-ust-taxonomy.yaml
wget "https://zenodo.org/record/2590742/files/README.md?download=1" -O README.md
