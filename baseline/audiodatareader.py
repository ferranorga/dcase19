import io
import torch
from torchvision import models, transforms, datasets
import torch.utils.data as data
import numpy as np
import math
import torch.nn as nn
import torch.nn.functional as F

import os
import os.path
import pickle
import hashlib
import librosa
from scipy.io import wavfile
import utils



class AudioReader(data.Dataset):
  
    def __init__(self, list_path, segment_length = 16000, max_length=-1):
        
        self.list_path = list_path
        self.database_path = os.path.dirname(list_path) + '/'
        self.segment_length = segment_length
        self.max_length = max_length

        self.audios = []
        self.class_ids =  {}
                 
        self.read_list_data()
         
        #esto es lo que hay que cambiar, aqu'i van las etiquetas [1,0,0,1,1,1]
        #para cada audio_path -> self.class_ids[audio_path] = [1,0,1,1,0,1,1,0]
        
        self.seeded = False
    
    def read_list_data(self):
        with open(self.list_path, 'r') as stream:
            for line in stream:
                file_path = line.strip().split()
                self.self.class_ids[file_path] = [1,0,1,1,0,1,1,0]
                self.audios.append(file_path)
                
        
    def __len__(self):
        return len(self.audios)


    def __getitem__(self, index):
        
        if not self.seeded:
            self.seeded = True
            np.random.seed(index)
            

          
        audio_id = self.audios[index]
        audio = self.load_audio(audio_id)
        
        class_id = self.class_ids[audio_id]
      
        audio -= audio.mean()
        max_val = np.max(np.abs(audio))
        audio /= max_val + 0.001
            
        audio = torch.FloatTensor(audio)
        
        #if self.max_length > 0:
            #audio = audio[0:(self.max_length*16000)]

        # Take segment
        #if self.segment_length > 0:
        #    if np.random.rand() > 0.5:
        #        audio = -audio
        #    if audio.size(0) >= self.segment_length:
        #        max_audio_start = audio.size(0) - self.segment_length
        #        audio_start = np.random.randint(0, max_audio_start)
        #        audio = audio[audio_start:audio_start+self.segment_length]
        #    else:
        #        audio = torch.nn.functional.pad(audio, (0, self.segment_length - audio.size(0)), 'constant').data
        #    
        #    if self.add_noise:
        #        alpha = np.random.uniform(low=0.75, high=0.98)
        #        beta = 1.0 - alpha
        #        silence_chunk = self.get_silence_chunk(audio.size(0))
        #        max_val = np.abs(silence_chunk[np.argpartition(np.abs(silence_chunk),-10)[-10:]]).mean()
        #        silence_chunk /= max_val + 0.001    
        #        audio_noise = audio * alpha + beta * torch.FloatTensor(silence_chunk)
        
        
        #print(torch.min(audio), torch.max(audio))    
        
        return [audio.unsqueeze(0), class_id]
        
    def load_audio(self, audio_name):
        
        audio_path = self.database_path + audio_name
        fs, audio = wavfile.read(audio_path)
        audio = audio / 2**15
        #audio, fs = librosa.load(audio_path)

        return audio
                 
    
    

